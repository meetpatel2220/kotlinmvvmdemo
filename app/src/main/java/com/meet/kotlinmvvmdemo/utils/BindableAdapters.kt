package com.meet.kotlinmvvmdemo.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject
import com.meet.kotlinmvvmdemo.recycleAc.HomeAdapter
import com.meet.kotlinmvvmdemo.recycleAc.RecycleViewModel

@BindingAdapter("assignList", "assignViewModel", requireAll = true)
fun bindList(recyclerView: RecyclerView, list: List<ListObject>?, viewModel: RecycleViewModel) {
    val adapter = getOrSetAdapter(recyclerView)
    adapter.update(list)
    adapter.recycleViewModel = viewModel
}

private fun getOrSetAdapter(recyclerView: RecyclerView): HomeAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is HomeAdapter) {
        recyclerView.adapter as HomeAdapter
    } else {
        val adapter = HomeAdapter()
        recyclerView.adapter = adapter
        adapter
    }
}