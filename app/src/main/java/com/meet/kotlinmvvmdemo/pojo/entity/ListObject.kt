package com.meet.kotlinmvvmdemo.pojo.entity


import com.google.gson.annotations.SerializedName

data class ListObject(
    @SerializedName("description")
    var description: String = "",
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("location")
    var location: String = "",
    @SerializedName("name")
    var name: String = ""
)