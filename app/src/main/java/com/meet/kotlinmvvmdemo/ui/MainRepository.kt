package com.meet.kotlinmvvmdemo.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.meet.kotlinmvvmdemo.network.RestClient
import com.meet.kotlinmvvmdemo.pojo.request.RequestDeleteData
import com.meet.kotlinmvvmdemo.pojo.request.RequestInsertData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

object MainRepository {

    private val responseInsertData_ = MutableLiveData<Boolean>()
    val responseInsertData: LiveData<Boolean>
        get() = responseInsertData_



    fun insertdata(requestInsertData: RequestInsertData) {
        val webService = RestClient.create()
        webService.insertData(requestInsertData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                if (it.status == "success")
                    responseInsertData_.postValue(true)
                else
                    responseInsertData_.postValue(false)
            }, {
                responseInsertData_.postValue(false)
            })

    }

}