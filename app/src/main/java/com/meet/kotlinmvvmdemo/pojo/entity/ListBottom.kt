package com.meet.kotlinmvvmdemo.pojo.entity


import com.google.gson.annotations.SerializedName

data class ListBottom(
   var description: String = "",
     var id: String? = null,
   var location: String = "",
    var name: String = ""
)