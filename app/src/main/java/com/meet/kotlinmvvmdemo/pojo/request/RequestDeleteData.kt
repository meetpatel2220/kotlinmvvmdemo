package com.meet.kotlinmvvmdemo.pojo.request


import com.google.gson.annotations.SerializedName

data class RequestDeleteData(
    @SerializedName("apikey")
    var apikey: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("userid")
    var userid: String = ""
)