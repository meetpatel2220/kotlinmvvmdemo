package com.meet.kotlinmvvmdemo.recycleAc

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.meet.kotlinmvvmdemo.network.RestClient
import com.meet.kotlinmvvmdemo.pojo.request.RequestDeleteData
import com.meet.kotlinmvvmdemo.pojo.request.RequestGetList
import com.meet.kotlinmvvmdemo.pojo.request.RequestInsertData
import com.meet.kotlinmvvmdemo.pojo.response.ResponseGetList
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

object RecycleRepository {
    private val TAG = "RecycleRepository"

    private val responseGetList_ = MutableLiveData<ResponseGetList>()
    val responseGetList: LiveData<ResponseGetList>
        get() = responseGetList_
    fun fetchList(requestGetList: RequestGetList) {
        val webService = RestClient.create()
        webService.getList(requestGetList)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                responseGetList_.postValue(it)
            }, {
                Log.e(TAG, "Error Occurred", it)
            })
    }


    private val responseDeleteData_ = MutableLiveData<Boolean>()
    val responseDeleteData: LiveData<Boolean>
        get() = responseDeleteData_

    fun deletedata(requestdeleteData: RequestDeleteData) {
        val webService = RestClient.create()
        webService.deleteData(requestdeleteData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                if (it.status == "success")
                    responseDeleteData_.postValue(true)
                else
                    responseDeleteData_.postValue(false)
            }, {
                responseDeleteData_.postValue(false)
            })

    }

}