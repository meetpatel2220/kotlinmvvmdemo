package com.meet.kotlinmvvmdemo.pojo.response


import com.google.gson.annotations.SerializedName

data class ResponseInsertData(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("message")
    var message: String = "",
    @SerializedName("status")
    var status: String = ""
)