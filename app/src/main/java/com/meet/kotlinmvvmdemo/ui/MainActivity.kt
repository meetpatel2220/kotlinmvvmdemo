package com.meet.kotlinmvvmdemo.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.meet.kotlinmvvmdemo.R
import com.meet.kotlinmvvmdemo.databinding.ActivityMain2Binding
import com.meet.kotlinmvvmdemo.recycleAc.RecycleAc


class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var dialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMain2Binding =
            DataBindingUtil.setContentView(this, R.layout.activity_main2)

        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        binding.apply {
            viewModel = mainViewModel
            lifecycleOwner = this@MainActivity
        }
        subscribeObservers()

        binding.button2.setOnClickListener {
          val intent= Intent(this,RecycleAc::class.java)
            startActivity(intent)
        }


    }

    private fun subscribeObservers() {

        mainViewModel.responseInsertData.observe(this){
            if(it){
                Toast.makeText(this, "Data Inserted Successfully", Toast.LENGTH_LONG)
                    .show()
            }
        }

        mainViewModel.isnextbuttonclick.observe(this){
            if(it){
                //intent
            }
        }

        mainViewModel.itembottom.observe(this){
            if(it!=null){
                Toast.makeText(this,it.name,Toast.LENGTH_LONG).show()
                openbottomsheet()
            }else{
                if(dialog.isShowing){
                    dialog.dismiss()
                }
            }
        }


    }

    private fun openbottomsheet() {

        dialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.bottomsheet_editdetail, null)
        dialog.setContentView(view)





        dialog.show()


    }
}