package com.meet.kotlinmvvmdemo.ui

import android.text.Editable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.meet.kotlinmvvmdemo.pojo.entity.ListBottom
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject
import com.meet.kotlinmvvmdemo.pojo.request.RequestDeleteData
import com.meet.kotlinmvvmdemo.pojo.request.RequestInsertData
import com.meet.kotlinmvvmdemo.utils.Constants

class MainViewModel : ViewModel() {

    val responseInsertData = MainRepository.responseInsertData as MutableLiveData<Boolean>

    val isnextbuttonclick = MutableLiveData<Boolean>()

    var itembottom = MutableLiveData<ListBottom?>()

    fun onItemClickbottom(item: ListBottom) {
        itembottom.postValue(item)

    }



     fun onbclick(
        name: Editable,
        location: Editable,
        description: Editable,
        price: Editable
    ) {


        onItemClickbottom(ListBottom(description.toString(),null,location.toString(),name.toString()))

    }


    fun onSaveClick(
        name: Editable,
        location: Editable,
        description: Editable,
        price: Editable,
    ) {

        MainRepository.insertdata(
            RequestInsertData(
                Constants.APIKEY,
                description.toString(),
                location.toString(),
                name.toString(),
                price.toString(),
                Constants.USERID
            )
        )
    }


}