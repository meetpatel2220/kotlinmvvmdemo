package com.meet.kotlinmvvmdemo.recycleAc

import android.text.Editable
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject
import com.meet.kotlinmvvmdemo.pojo.request.RequestDeleteData
import com.meet.kotlinmvvmdemo.pojo.request.RequestGetList
import com.meet.kotlinmvvmdemo.pojo.request.RequestInsertData
import com.meet.kotlinmvvmdemo.ui.MainRepository
import com.meet.kotlinmvvmdemo.utils.Constants
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class RecycleViewModel : ViewModel() {

    val list: LiveData<List<ListObject>> = HomeUseCase.list
    val responseDeleteData = RecycleRepository.responseDeleteData as MutableLiveData<Boolean>

    fun onItemdelete(item: ListObject) {
        RecycleRepository.deletedata(RequestDeleteData(Constants.APIKEY, item.id!!, Constants.USERID))
    }
    init {
        fetchNewList()
    }
    fun fetchNewList() {
        RecycleRepository.fetchList(RequestGetList())
    }


}