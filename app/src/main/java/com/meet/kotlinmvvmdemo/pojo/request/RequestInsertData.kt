package com.meet.kotlinmvvmdemo.pojo.request


import com.google.gson.annotations.SerializedName

data class RequestInsertData(
    @SerializedName("apikey")
    var apikey: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("location")
    var location: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("price")
    var price: String = "",
    @SerializedName("userid")
    var userid: String = ""
)