package com.meet.kotlinmvvmdemo.pojo.request


import com.google.gson.annotations.SerializedName

data class RequestGetList(
    @SerializedName("apikey")
    var apikey: String = "164310030161efb88d2d888",
    @SerializedName("userid")
    var userid: String = "1",
    @SerializedName("storeid")
    var storeid: String = "158",
)