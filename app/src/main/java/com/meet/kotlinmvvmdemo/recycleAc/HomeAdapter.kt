package com.meet.kotlinmvvmdemo.recycleAc

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.meet.kotlinmvvmdemo.R
import com.meet.kotlinmvvmdemo.databinding.ItemListBinding
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject
import com.meet.kotlinmvvmdemo.utils.DiffUtilCallbacks

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    private var itemList: List<ListObject> = listOf()
    lateinit var recycleViewModel: RecycleViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_list,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    override fun getItemCount(): Int = itemList.size

    fun update(updated: List<ListObject>?) {

        val oldList = itemList
        itemList = updated ?: listOf()
        val diff = DiffUtil.calculateDiff(DiffUtilCallbacks(oldList,itemList))
        diff.dispatchUpdatesTo(this)
    }
    inner class ViewHolder(private val binding: ItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(currentItem: ListObject) {
            binding.item = currentItem
            binding.viewModel = recycleViewModel
            binding.executePendingBindings()
        }
    }
}