package com.meet.kotlinmvvmdemo.pojo.response


import com.google.gson.annotations.SerializedName
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject

data class ResponseGetList(
    @SerializedName("data")
    var `data`: Map<Int, ListObject> = mapOf(),
    @SerializedName("status")
    var status: String = ""
)