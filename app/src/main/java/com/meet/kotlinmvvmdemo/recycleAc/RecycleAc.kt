package com.meet.kotlinmvvmdemo.recycleAc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.meet.kotlinmvvmdemo.R
import com.meet.kotlinmvvmdemo.databinding.ActivityRecycleBinding
import com.meet.kotlinmvvmdemo.ui.MainViewModel

class RecycleAc : AppCompatActivity() {
    private lateinit var recyclerViewModel: RecycleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityRecycleBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_recycle)
        recyclerViewModel = ViewModelProvider(this)[RecycleViewModel::class.java]
        binding.apply {
            recycleviewModel = recyclerViewModel
            lifecycleOwner = this@RecycleAc
        }
        subscribeObservers()
    }
    private fun subscribeObservers() {

        recyclerViewModel.responseDeleteData.observe(this){
            if(it){
                recyclerViewModel.fetchNewList()
                Toast.makeText(this,"data deleted",Toast.LENGTH_LONG).show()
                recyclerViewModel.responseDeleteData.value=false
            }
        }
        recyclerViewModel.list.observe(this){
            if(it==null){

            }else{

            }
        }
    }


}