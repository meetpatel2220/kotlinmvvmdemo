package com.meet.kotlinmvvmdemo.utils

import androidx.recyclerview.widget.DiffUtil
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject

class DiffUtilCallbacks(private val oldList: List<ListObject>, private val newList: List<ListObject>): DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }
}