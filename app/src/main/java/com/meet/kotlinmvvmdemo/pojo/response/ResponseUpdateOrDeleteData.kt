package com.meet.kotlinmvvmdemo.pojo.response


import com.google.gson.annotations.SerializedName

data class ResponseUpdateOrDeleteData(
    @SerializedName("data")
    var `data`: String = "",
    @SerializedName("status")
    var status: String = ""
)