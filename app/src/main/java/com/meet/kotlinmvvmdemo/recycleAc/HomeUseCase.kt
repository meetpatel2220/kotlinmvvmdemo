package com.meet.kotlinmvvmdemo.recycleAc

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.meet.kotlinmvvmdemo.pojo.entity.ListObject
import com.meet.kotlinmvvmdemo.pojo.response.ResponseGetList

object HomeUseCase {
    private val rawList: LiveData<ResponseGetList>
        get() = RecycleRepository.responseGetList

    val list: LiveData<List<ListObject>> = formatList()

    private fun formatList(): LiveData<List<ListObject>> {
        val data = rawList
        val ans = data.map {
            convertToList(it)
        }
        return ans
    }

    private fun convertToList(responseGetList: ResponseGetList): List<ListObject> {
        val ans = mutableListOf<ListObject>()
        for (i in responseGetList.data) {
            ans.add(i.value)
        }
        return ans
    }

}