package com.meet.kotlinmvvmdemo.network

import com.meet.kotlinmvvmdemo.pojo.request.RequestDeleteData
import com.meet.kotlinmvvmdemo.pojo.request.RequestGetList
import com.meet.kotlinmvvmdemo.pojo.request.RequestInsertData
import com.meet.kotlinmvvmdemo.pojo.response.ResponseGetList
import com.meet.kotlinmvvmdemo.pojo.response.ResponseInsertData
import com.meet.kotlinmvvmdemo.pojo.response.ResponseUpdateOrDeleteData
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface Services {

    @POST("dummy-list")
    fun getList(@Body requestBody: RequestGetList): Observable<ResponseGetList>

//    @POST("dummy-update")
//    fun updateData(@Body requestBody: RequestUpdateData): Observable<ResponseUpdateOrDeleteData>

    @POST("dummy-insert")
    fun insertData(@Body requestInsertData: RequestInsertData): Observable<ResponseInsertData>

    @POST("dummy-delete")
    fun deleteData(@Body requestDeleteData: RequestDeleteData): Observable<ResponseUpdateOrDeleteData>

}